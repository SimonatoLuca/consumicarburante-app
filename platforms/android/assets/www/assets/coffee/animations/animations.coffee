.factory('animazioni', ->
		
		header:
			image: null
			title: null
			line: null
			info:
				container: null

		container:
			listEl: null

	).animation('.animation', ($timeout, animazioni) ->

		enter: (element, done) ->

			$timeout ->

				if !animazioni.header.image then animazioni.header.image = document.getElementsByClassName 'profile-img'
				if !animazioni.header.title then animazioni.header.title = document.getElementsByTagName 'h1'
				if !animazioni.header.line then animazioni.header.line = document.getElementsByClassName 'full-border'
				if !animazioni.header.info.container then animazioni.header.info.container = document.getElementsByClassName 'second-container'

				if !animazioni.container.listEl then animazioni.container.listEl = document.getElementsByClassName 'list-el'

				TweenMax.fromTo animazioni.header.image, 1, {scale:0}, {scale:1, ease:Elastic.easeOut.config(1,0.4)}
				TweenMax.fromTo animazioni.header.title, 1, {y:-20}, {y:0, ease:Elastic.easeOut.config(1,0.4)}
				TweenMax.fromTo animazioni.header.line, 1, {scaleX:0}, {scaleX:1, ease:Elastic.easeOut.config(1,0.4)}
				TweenMax.fromTo animazioni.header.info.container, 1, {y:-20}, {y:0, ease:Elastic.easeOut.config(1,0.4)}

				TweenMax.staggerFromTo animazioni.container.listEl, 1, {y:-20, opacity:0}, {y:0, opacity:1, ease:Elastic.easeOut.config(1,0.4)}, 0.1

	).animation('.deleter-animation', ->

		add: (element) ->

			TweenMax.to element, 0.4, {x:0, opacity:0, delay: 0.2, ease:Elastic.easeOut.config(1,0.4)}

		remove: (element) ->

			TweenMax.to element, 0.4, {x:-50, opacity:0}

	)