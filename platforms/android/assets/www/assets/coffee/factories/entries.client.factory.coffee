'use strict'

angular.module('entries')
.factory('entriesFactory',['$http', '$location', ($http, $location) ->

	id: null
	data: []
	get: (userId, carId, id) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/' + id
		$http.get(url)
	create: (userId, carId, contenuto) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/entries'
		$http.post(url, contenuto)
	del: (userId, carId, id) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/' + id
		$http.delete(url)
	update: (userId, carId, id, contenuto) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/' + id
		$http.put(url, contenuto)

])