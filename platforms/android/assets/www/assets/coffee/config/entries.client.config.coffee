'use strict'

angular.module('entries')
.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->

	$routeProvider
	.when '/cars/:carId/add-entry',
		templateUrl: 'assets/templates/pages/add-entry.html'
		controller: 'entryAddCtrl'
	.when '/cars/:carId/:entryId/edit-entry',
		templateUrl: 'assets/templates/pages/edit-entry.html'
		controller: 'entryEditCtrl'

])