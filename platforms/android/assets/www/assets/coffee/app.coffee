'use strict'

angular.module('app', [
	'ngRoute'
	'ngAnimate'
	'ngTouch'
	'oauth.io'
	'users'
	'cars'
	'entries'
	])