'use strict'

angular.module('entries')
.controller('entriesListCtrl', ['$scope', '$routeParams', '$filter', 'userFactory', 'carsFactory', 'entriesFactory', ($scope, $routeParams, $filter, userFactory, carsFactory, entriesFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id
	$scope.carId = carId = $routeParams.carId

	carFound = $filter('filter')(localUser.cars, {_id: carId}, true)

	if carFound
		$scope.car = car = carFound[0]
		console.log car
	else
		console.log 'non ha funzionato'

	if !userFactory.cambiato

		$scope.entries = car.entries

		if car.entries.length > 0

			$scope.addMessage = 'add an entry'

		else

			$scope.addMessage = 'add your first entry'

		console.log car.entries

	else

		userFactory.load(userId)
			.success (data) ->

				window.localStorage.setItem 'user', JSON.stringify data
				localUser = JSON.parse window.localStorage.getItem 'user'

				carFound = $filter('filter')(localUser.cars, {_id: carId}, true)

				if carFound
					$scope.car = car = JSON.stringify(carFound[0]);
				else
					console.log 'non ha funzionato'

				$scope.entries = car.entries
				$scope.addMessage = 'add an entry'

				console.log car.entries

			.error (error) ->
				console.log error
				console.log 'errore nel caricare i dati delle auto'

]).controller('entryListElCtrl', ['$scope', 'userFactory', 'carsFactory', 'entriesFactory', ($scope, userFactory, carsFactory, entriesFactory) ->

	$scope.deleterAperto = false

	$scope.elementoVisibile = true

	$scope.apriCancella = -> $scope.deleterAperto = true

	$scope.chiudiCancella = -> $scope.deleterAperto = false

	$scope.deleteCar = (id) ->
		entriesFactory.del(id)
			.success( ->
				$scope.elementoVisibile = false
				userFactory.cambiato = true
			)

]).controller('entryAddCtrl', ['$scope', '$window', '$routeParams', '$filter', 'userFactory', 'entriesFactory',  ($scope, $window, $routeParams, $filter, userFactory, entriesFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id
	carId = $routeParams.carId

	carFound = $filter('filter')(localUser.cars, {_id: carId}, true)

	if carFound
		car = carFound[0]
	else
		console.log 'non ha funzionato'

	$scope.carBifuel = car.bifuel
	$scope.alPrimaria = car.alPrimaria
	$scope.alSecondaria = car.alSecondaria

	$scope.create = ->

		liters = @paid/@price
		mediaKilometro = @kilometers/liters
		fuel = if @fuel then @fuel else car.alPrimaria

		entry =
			fuel: fuel
			kilometers: @kilometers
			price: @price
			paid: @paid
			full: @full
			liters: liters
			mediaKilometro: mediaKilometro

		entriesFactory.create(userId, carId, entry)
			.success ->
				userFactory.cambiato = true
				console.log 'rifornimento inserito!'
				$window.location.href = '#/cars/' + carId
			.error ->
				console.log 'luca sei scemo'

]).controller('entryEditCtrl', ['$scope', '$window', '$routeParams', '$filter', 'userFactory', 'entriesFactory',  ($scope, $window, $routeParams, $filter, userFactory, entriesFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id
	carId = $routeParams.carId
	entryId = $routeParams.entryId

	carFound = $filter('filter')(localUser.cars, {_id: carId}, true)

	if carFound
		car = carFound[0]
	else
		console.log 'non ha funzionato'

	$scope.carBifuel = car.bifuel
	$scope.alPrimaria = car.alPrimaria
	$scope.alSecondaria = car.alSecondaria

	$scope.findOne = ->

		entriesFactory.get(userId, carId, entryId)
			.success (data)->
				$scope.entry = data
				console.log $scope.entry
			.error ->
				console.log 'errore nel caricare questa entry'

	$scope.update = ->

		console.log $scope.entry

		liters = $scope.entry.paid/$scope.entry.price
		mediaKilometro = $scope.entry.kilometers/liters
		if $scope.entry.fuel then fuel = $scope.entry.fuel else fuel = car.alPrimaria

		console.log 'liters: ' + liters + '\nmediaKilometro: ' + mediaKilometro + '\nfuel: ' + fuel

		entry =
			fuel: fuel
			kilometers: $scope.entry.kilometers
			price: $scope.entry.price
			paid: $scope.entry.paid
			full: $scope.entry.full
			liters: liters
			mediaKilometro: mediaKilometro

		entriesFactory.update(userId, carId, entryId, entry)
			.success ->
				userFactory.cambiato = true
				console.log 'rifornimento inserito!'
				$window.location.href = '#/cars/' + carId
			.error ->
				console.log 'luca sei scemo'

])