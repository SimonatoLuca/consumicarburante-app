// Generated by CoffeeScript 1.9.3
(function() {
  'use strict';
  angular.module('users').factory('userFactory', [
    '$http', '$location', function($http, $location) {
      return {
        cambiato: true,
        load: function(id) {
          var url;
          url = 'http://cc.opificiolamantinianonimi.com/api/users/' + id;
          return $http.get(url);
        }
      };
    }
  ]);

}).call(this);


//# sourceMappingURL=users.client.factory.js.map
