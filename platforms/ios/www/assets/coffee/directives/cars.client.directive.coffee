'use strict'

angular.module('cars')
.directive('carHeader', ->

	restrict: 'A',
	templateUrl: 'assets/templates/directives/car-header.html'
	controller: 'carHeaderCtrl'

).directive('carsList', ->

	restrict: 'A',
	templateUrl: 'assets/templates/directives/cars-list.html'
	controller: 'carListCtrl'

)