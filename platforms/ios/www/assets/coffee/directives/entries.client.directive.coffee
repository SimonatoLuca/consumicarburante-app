'use strict'

angular.module('entries')
.directive('entriesList', ->

	restrict: 'A',
	templateUrl: 'assets/templates/directives/entries-list.html'
	controller: 'entriesListCtrl'

)