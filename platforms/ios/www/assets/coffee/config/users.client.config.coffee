'use strict'

angular.module('users')
.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->

	$routeProvider
	.when '/', 
		templateUrl: 'assets/templates/pages/loading.html'
		controller: 'loadingCtrl'

])