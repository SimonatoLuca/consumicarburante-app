'use strict'

angular.module('cars')
.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->

	$routeProvider
	.when '/cars', 
		templateUrl: 'assets/templates/pages/cars.html'
		controller: 'carsCtrl'
	.when '/cars/add-car',
		templateUrl: 'assets/templates/pages/add-car.html'
		controller: 'carAddCtrl'
	.when '/cars/:carId',
		templateUrl: 'assets/templates/pages/car.html'
		controller: 'carCtrl'
	.when '/cars/:carId/edit-car',
		templateUrl: 'assets/templates/pages/edit-car.html'
		controller: 'carEditCtrl'

])