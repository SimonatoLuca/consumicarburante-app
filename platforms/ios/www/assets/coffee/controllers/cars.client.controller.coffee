'use strict'

angular.module('cars')
.controller('carsCtrl', ['$scope', '$routeParams', '$filter', 'userFactory', 'carsFactory', ($scope, $routeParams, $filter, userFactory, carsFactory) ->
	
	console.log 'siamo in cars'

	localUser = window.localStorage.getItem 'user'

	if localUser

		if userFactory.cambiato

			user = JSON.parse localUser
			console.log user
			userId = user._id
			userFactory.load(userId).success (data) ->
				window.localStorage.setItem 'user', JSON.stringify data
				localUser = window.localStorage.getItem 'user'
				userFactory.data = JSON.parse localUser

		else

			userFactory.data = JSON.parse localUser

	else

		$window.location.href = '#/'

]).controller('carCtrl', ['$scope', '$routeParams', '$filter', 'userFactory', 'carsFactory', ($scope, $routeParams, $filter, userFactory, carsFactory) ->
	
	console.log 'siamo in car'

	localUser = window.localStorage.getItem 'user'

	if localUser

		if userFactory.cambiato

			user = JSON.parse localUser
			userId = user._id
			userFactory.load(userId).success (data) ->
				window.localStorage.setItem 'user', JSON.stringify data
				localUser = window.localStorage.getItem 'user'
				userFactory.data = JSON.parse localUser

		else

			userFactory.data = JSON.parse localUser

	else

		$window.location.href = '#/'

]).controller('carHeaderCtrl', ['$scope', '$routeParams', '$filter', 'userFactory', 'carsFactory', ($scope, $routeParams, $filter, userFactory, carsFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id
	carId = $routeParams.carId
	cars = localUser.cars

	if cars

		carFound = $filter('filter')(localUser.cars, {_id: carId}, true)

		if carFound
			$scope.car = car = carFound[0]
		else
			console.log 'non ha funzionato'

	else

		userFactory.load(userId).success (data) ->
				window.localStorage.setItem 'user', JSON.stringify data
				localUser = JSON.parse window.localStorage.getItem 'user'

				carFound = $filter('filter')(localUser.cars, {_id: carId}, true)

				if carFound
					$scope.car = car = carFound[0]
				else
					console.log 'non ha funzionato'

	kmTotale = 0
	paidTotale = 0
	totaleMedie = 0
	mediaTotale = totaleMedie/car.entries.length

	for entry in car.entries
		paidTotale = paidTotale+entry.paid
		kmTotale = kmTotale+entry.kilometers
		mediaTotale = totaleMedie+entry.mediaKilometro

	$scope.kmTotale = kmTotale
	$scope.mediaTotale = parseInt(mediaTotale)
	$scope.paidTotale = paidTotale

]).controller('carListCtrl',['$scope', '$filter', 'userFactory', ($scope, $filter, userFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	if !userFactory.cambiato

		if localUser.cars.length > 0

			$scope.cars = localUser.cars
			$scope.addMessage = 'add a car'

		else

			$scope.addMessage = 'add your first car'

	else

		userFactory.load(localUser.id)
			.success (data) ->

				if data.cars

					window.localStorage.setItem 'user', JSON.stringify data
					localUser = JSON.parse window.localStorage.getItem 'user'

					$scope.cars = localUser.cars

					userFactory.cambiato = false
					$scope.addMessage = 'add a car'

				else
					
					userFactory.cambiato = false
					$scope.addMessage = 'add your first car'

			.error (error) ->
				console.log error
				console.log 'errore nel caricare i dati delle auto'

]).controller('carListElCtrl',['$scope', '$filter', 'userFactory', 'carsFactory', ($scope, $filter, userFactory, carsFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id

	$scope.deleterAperto = false

	$scope.elementoVisibile = true

	$scope.apriCancella = -> $scope.deleterAperto = true

	$scope.chiudiCancella = -> $scope.deleterAperto = false

	$scope.deleteCar = (id) ->
		carsFactory.del(userId, id)
			.success( ->
				$scope.elementoVisibile = false
				userFactory.cambiato = true
			)

	for car in localUser.cars

		valoreSommato = 0

		if car.bifuel

			alPrimaria = car.alPrimaria
			alSecondaria = car.alSecondaria

			entriesAlPrimaria = $filter('filter')(car.entries, {fuel: alPrimaria}, true)
			entriesAlSecondaria = $filter('filter')(car.entries, {fuel: alSecondaria}, true)

			if entriesAlPrimaria.length > entriesAlSecondaria.length
				entries = entriesAlPrimaria
				console.log 'per la macchina ' + car.modello + ' usiamo la alPrimaria perchè ha ' + entriesAlPrimaria.length + ' invece di ' + entriesAlSecondaria.length
			else
				entries = entriesAlSecondaria
				console.log 'per la macchina ' + car.modello + ' usiamo la alSecondaria perchè ha ' + entriesAlSecondaria.length + ' invece di ' + entriesAlPrimaria.length

			console.log entries

		else

			entries = car.entries


		for entry in entries

			valoreSommato = valoreSommato + entry.mediaKilometro

		car.mediaLitri = parseInt(valoreSommato/car.entries.length)


]).controller('carAddCtrl',['$scope', '$window', 'userFactory', 'carsFactory', ($scope, $window, userFactory, carsFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id

	$scope.create = ->

		car =
			marca: @marca
			modello: @modello
			bifuel: @bifuel
			alPrimaria: @alPrimaria
			alSecondaria: @alSecondaria

		carsFactory.create(userId, car)
			.success ->
				userFactory.cambiato = true
				console.log 'macchina inserita!'
				$window.location.href = '#/cars'
			.error ->
				console.log 'luca sei scemo'

]).controller('carEditCtrl',['$scope', '$window', '$routeParams', 'userFactory', 'carsFactory', ($scope, $window, $routeParams, userFactory, carsFactory) ->

	localUser = JSON.parse window.localStorage.getItem 'user'

	userId = localUser._id
	carId = $routeParams.carId

	$scope.findOne = ->

		carsFactory.get(userId, carId)
			.success (data)->
				$scope.car = data
			.error ->
				console.log 'errore nel caricare questa auto'

	$scope.update = ->

		car =
			marca: $scope.car.marca
			modello: $scope.car.modello
			bifuel: $scope.car.bifuel
			alPrimaria: $scope.car.alPrimaria
			alSecondaria: $scope.car.alSecondaria

		carsFactory.update(userId, carId, car)
			.success ->
				userFactory.cambiato = true
				console.log 'macchina aggiornata!'
				$window.location.href = '#/cars'
			.error ->
				console.log 'luca sei scemo'

])