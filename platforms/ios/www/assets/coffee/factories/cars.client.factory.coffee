'use strict'

angular.module('cars')
.factory('carsFactory',['$http', '$location', ($http, $location) ->

	id: null
	data: null
	entries: []
	cambiata: false
	getAll: (userId, id) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/cars'
		$http.get(url)
	get: (userId, id) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + id
		$http.get(url)
	create: (userId, contenuto) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/cars'
		$http.post(url, contenuto)
	del: (userId, id) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + id
		$http.delete(url)
	update: (userId, id, contenuto) ->
		port = if $location.host().indexOf('opificio') > 0 then '' else ':3000'
		url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + id
		$http.put(url, contenuto)

])