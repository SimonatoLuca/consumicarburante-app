'use strict'

angular.module('users')
.factory('authFactory', ['$http', ($http) ->
	findOrCreate: (user) ->
		url = 'http://cc.opificiolamantinianonimi.com/api/users'
		$http.post(url, user)
])