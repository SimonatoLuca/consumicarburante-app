angular.module('oauth.io')
.provider 'OAuth', ->
  this.publicKey = ''
  this.handlers = {}
  this.setPublicKey = (key) ->
    this.publicKey = key
  this.setHandler = (method, handler) ->
    this.handlers[method] = handler
  provider = this
  this.$get = ['$window', 'OAuthData','$injector', ($window, OAuthData, $injector) ->
      function OAuth() {
        this.popup = function (method) {
          $window.OAuth.popup(method, (error, result) ->
            if !error
              if provider.handlers[method]
                OAuthData.result = result
                $injector.invoke provider.handlers[method]
          )
        }
        $window.OAuth.initialize(provider.publicKey)
      }
      return new OAuth();
    }
  ];
}).service 'OAuthData', OAuthData() ->