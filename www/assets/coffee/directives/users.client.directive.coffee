'use strict'

angular.module('users')
.directive('userHeader', ->

	restrict: 'A'
	templateUrl: 'assets/templates/directives/user-header.html'
	controller: 'userHeaderCtrl'
	
)