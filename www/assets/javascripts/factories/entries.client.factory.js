// Generated by CoffeeScript 1.9.3
(function() {
  'use strict';
  angular.module('entries').factory('entriesFactory', [
    '$http', '$location', function($http, $location) {
      return {
        id: null,
        data: [],
        get: function(userId, carId, id) {
          var port, url;
          port = $location.host().indexOf('opificio') > 0 ? '' : ':3000';
          url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/' + id;
          return $http.get(url);
        },
        create: function(userId, carId, contenuto) {
          var port, url;
          port = $location.host().indexOf('opificio') > 0 ? '' : ':3000';
          url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/entries';
          return $http.post(url, contenuto);
        },
        del: function(userId, carId, id) {
          var port, url;
          port = $location.host().indexOf('opificio') > 0 ? '' : ':3000';
          url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/' + id;
          return $http["delete"](url);
        },
        update: function(userId, carId, id, contenuto) {
          var port, url;
          port = $location.host().indexOf('opificio') > 0 ? '' : ':3000';
          url = 'http://cc.opificiolamantinianonimi.com/api/' + userId + '/' + carId + '/' + id;
          return $http.put(url, contenuto);
        }
      };
    }
  ]);

}).call(this);


//# sourceMappingURL=entries.client.factory.js.map
